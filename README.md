# interface-lammps-mlip-3

An interface between LAMMPS and MLIP (version 3).
MLIP, Machine Learning Interatomic Potentials, is a software distributed
by the group of A. Shapeev from Skoltech (Moscow).
MLIP is an open-source software package available at https://gitlab.com/ashapeev/mlip-3.
After you have downloaded MLIP you can follow the instructions below to
make the MLIP-LAMMPS interaface.

Note: current version of the MLIP-LAMMPS interface works only with 
LAMMPS-2022 or later. Please, download this version of LAMMPS. 

## Installation

In order to install a MLIP plugin into LAMMPS:

0. Clone MLIP-LAMMPS interface

```git clone https://gitlab.com/ivannovikov/interface-lammps-mlip-3.git```

1. Clone the 2022 version (or later) of the LAMMPS code (we recommend using a previously uncompiled version of the code) 

```git clone -b stable https://github.com/lammps/lammps.git mylammps```
 
and place it in the same folder as `interface-lammps-mlip-3/`

2. Compile `lib_mlip_interface.a` in the MLIP folder

```make libinterface```

and place it in `interface-lammps-mlip-3/`

3. (optional) If additional LAMMPS packages are necessary they should be included at the end of the preinstall.sh file

```make yes-<package>```

e.g.

```make yes-manybody```

4. Compile LAMMPS  

```./install.sh <path-to-lammps> <lammps-target>```

e.g.

```./install.sh ../mylammps mpi```

If everything goes fine, the lammps binary will be copied to this folder

### Combination of LAMMPS and MLIP settings that work:

* MLIP and LAMMPS with default compiler (serial version) `make serial` 
* MLIP and LAMMPS with default compiler (parallel version) `make mpi` 
* MLIP with the GNU compiler and LAMMPS with `make g++_serial` 
* MLIP with the GNU compiler and mpich and LAMMPS with `make g++_mpich` 
* MLIP with Intel compiler and intel MPI and LAMMPS with `make intel_cpu_intelmpi`
* If you'd like to build with Intel and no MPI then you need to find a way to link LAMMPS with MKL
